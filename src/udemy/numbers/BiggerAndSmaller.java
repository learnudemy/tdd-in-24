package udemy.numbers;

public class BiggerAndSmaller {

    private int bigger = Integer.MIN_VALUE;
    private int smaller = Integer.MAX_VALUE;

    public void find(int[] numbers){
        for (int number: numbers) {
            if(number > bigger){
                bigger = number;
            }
            if (number < smaller){
                smaller = number;
            }
        }
    }

    public int getBigger() {
        return bigger;
    }

    public int getSmaller() {
        return smaller;
    }
}
