package udemy.discount;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class BasketBuilder {
    private List<Item> items;
    private ZonedDateTime date;

    public BasketBuilder(){
        this.items = new ArrayList<Item>();
        this.date = ZonedDateTime.now();
    }

    public BasketBuilder w(String name, double price) {
        return w(name, 1, price);
    }

    public BasketBuilder w(String name, int qty, double price){
        items.add(new Item(name, qty, price));
        return this;
    }

    public Basket build() {
        return new Basket(items, date);
    }
}
