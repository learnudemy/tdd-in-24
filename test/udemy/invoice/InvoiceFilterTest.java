package udemy.invoice;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class InvoiceFilterTest {

    private InvoiceFilter filter;

    private InvoiceRepository repo = Mockito.mock(InvoiceRepository.class);

    @Before
    public void setUp(){
        repo = Mockito.mock(InvoiceRepository.class);
        filter = new InvoiceFilter(repo);
    }

    @Test
    public void filterInvoicesHigherThan2000() {
        Invoice inv1 = new Invoice(Calendar.getInstance(), "MAURICIO", 5000);
        Invoice inv2 = new Invoice(Calendar.getInstance(), "MAURICIO", 200);

        when(repo.all())
                .thenReturn(Arrays.asList(inv1, inv2));

        List<Invoice> result = filter.filter();

        Assert.assertEquals(1, result.size());
        Assert.assertEquals(inv1, result.get(0));
    }

    @Test
    public void filterMicrosoftInvoices(){
        Invoice inv1 = new Invoice(Calendar.getInstance(), "MICROSOFT", 1000);

        when(repo.all())
                .thenReturn(Arrays.asList(inv1));

        List<Invoice> result = filter.filter();

        Assert.assertEquals(1, result.size());
        Assert.assertEquals(inv1, result.get(0));
    }
}